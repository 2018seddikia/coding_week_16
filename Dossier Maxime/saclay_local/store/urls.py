from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.urls import include, path
import store
from store import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index)
    
]
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
] + urlpatterns
