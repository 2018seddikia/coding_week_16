from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
import store
from store import models
# Create your views here.
def index(request):
    reponse = HttpResponse()
    chaine = '<ul>'
    for dic in models.PRODUCTS :
        chaine += '<li>'+dic['name']+'</li>'
    chaine  += '</ul>'
    reponse.write(chaine)
    return reponse

def bienvenue(request):
    return HttpResponse("<center><h0>BIENVENUE</h0></center>")