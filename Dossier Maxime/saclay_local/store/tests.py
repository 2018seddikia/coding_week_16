from django.test import TestCase
from django.urls import resolve
from store.views import index
from django.http import HttpResponse, HttpRequest
# Create your tests here.

class StorePageTest(TestCase):

    def test_root_url_resolves_to_store_page_view(self):
        found = resolve('/store/')
        self.assertEqual(found.func, index)

    def url_exists(self):
        formulaire = HttpResponse.__init__('http://127.0.0.1:8000/store/')
        self.assertNotEqual(formulaire.status_code,404)
