from django.db import models

# Create your models here.
PRODUCERS = {
  'ferme_viltain': {'name': 'Ferme_de_Viltain'},
}


PRODUCTS = [
  {'name': 'yaourth vanille', 'producers': [PRODUCERS['ferme_viltain']]},
  {'name': 'yaourth marron', 'producers': [PRODUCERS['ferme_viltain']]},
  {'name': 'jus de pomme', 'producers': [PRODUCERS['ferme_viltain']]}
]
